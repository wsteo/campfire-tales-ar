using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;

public class InputManager : MonoBehaviour
{
    private PlayerInputs _inputs;

    public PlayerInputs Inputs => _inputs;

    public delegate void StartTouchEvent(Vector2 position, float time);
    public event StartTouchEvent OnStartTouch;

    public delegate void EndTouchEvent(Vector2 position, float time);
    public event EndTouchEvent OnEndTouch;

    private void Awake()
    {
        _inputs = new PlayerInputs();
    }
    void Start()
    {
        _inputs.Touch.TouchPress.started += ctx => StartTouch(ctx);
        _inputs.Touch.TouchPress.started += ctx => EndTouch(ctx);
    }

    private void OnEnable()
    {
        _inputs.Enable();
        EnhancedTouchSupport.Enable();
        TouchSimulation.Enable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += FingerDown;
    }

    private void OnDisable()
    {
        _inputs.Disable();
        EnhancedTouchSupport.Enable();
        TouchSimulation.Disable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= FingerDown;
    }

    private void StartTouch(InputAction.CallbackContext context)
    {
        // Debug.Log("Touch started " + inputs.Touch.TouchPosition.ReadValue<Vector2>());
        if (OnStartTouch != null)
        {
            OnStartTouch(_inputs.Touch.TouchPosition.ReadValue<Vector2>(), (float)context.startTime);
        }
    }

    private void EndTouch(InputAction.CallbackContext context)
    {
        // Debug.Log("Touch ended");
        if (OnEndTouch != null)
        {
            OnEndTouch(_inputs.Touch.TouchPosition.ReadValue<Vector2>(), (float)context.time);
        }
    }

    public void FingerDown(Finger finger)
    {
        if (OnStartTouch != null) OnStartTouch(finger.screenPosition, Time.time);
    }

    // private void Update()
    // {
    //     Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches);

    //     foreach (UnityEngine.InputSystem.EnhancedTouch.Touch touch in UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches)
    //     {
    //         Debug.Log(touch.phase == UnityEngine.InputSystem.TouchPhase.Began);
    //     }

    // }
}