using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneAndLevelManager : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private List<GameSceneSO> _availableLevelList;
    [SerializeField] private List<GameSceneSO> _menuList;
    [SerializeField] private GameSceneSO _currentLoadedScene;
    [SerializeField] private GameSceneSO _nextScene;

    [Header("UI")]
    [SerializeField] private Canvas _loadingSceneCanvas;
    [SerializeField] private Slider _loadingBar;
    [SerializeField] private FloatVariable _loadingProgress;

    private void OnEnable()
    {
        SceneInitialization.onSceneLoadedEvent += UpdateLevelInformation;
        ExitPoint.onPlayerWinEvent += PauseGame;
    }

    private void OnDisable()
    {
        SceneInitialization.onSceneLoadedEvent -= UpdateLevelInformation;
        ExitPoint.onPlayerWinEvent -= PauseGame;
    }

    private void UpdateLevelInformation(GameSceneSO gameSceneSO)
    {
        _currentLoadedScene = gameSceneSO;
        _nextScene = gameSceneSO.nextLevel;
    }

    #region SceneManagerFunctionality
    public void LoadScene(GameSceneSO targetLevelSO)
    {
        SceneManager.LoadSceneAsync(targetLevelSO.sceneName, LoadSceneMode.Additive);
    }

    IEnumerator LoadLevelWithProgressBar(GameSceneSO targetLevelSO)
    {
        yield return null;

        _loadingSceneCanvas.enabled = true;
        
        float progress;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(targetLevelSO.sceneName, LoadSceneMode.Additive);

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            progress = Mathf.Clamp01(asyncOperation.progress / 0.9f);
            _loadingProgress.RuntimeValue = progress;
            _loadingBar.value = progress;
            Debug.Log(asyncOperation.progress);

            if (asyncOperation.progress >= 0.9f)
            {
                yield return new WaitForSeconds(3f);
                _loadingSceneCanvas.enabled = false;
                asyncOperation.allowSceneActivation = true;
            }
            
            yield return null;
        }
    }

    public void UnloadScene(GameSceneSO targetLevelSO)
    {
        SceneManager.UnloadSceneAsync(targetLevelSO.sceneName);
    }

    public bool IsSceneLoaded(GameSceneSO targetLevelSO)
    {
        Scene loadedScene = SceneManager.GetSceneByName(targetLevelSO.name);
        if (loadedScene.isLoaded)
        {
            return true;
        }
        return false;
    }
    #endregion

    #region HelperMethodsForSceneLoading

    public void LoadNextLevel()
    {
        onRestartLevelEvent?.Invoke(false);
        UnloadScene(_currentLoadedScene);
        StartCoroutine(LoadLevelWithProgressBar(_nextScene));
    }
    
    public delegate void RestartLevelEvent(bool hasRestart);
    public static event RestartLevelEvent onRestartLevelEvent;
    
    public void RestartLevel()
    {
        onRestartLevelEvent?.Invoke(true);
        UnloadScene(_currentLoadedScene);
        LoadScene(_currentLoadedScene);
    }

    public void NewGame()
    {
        if (IsSceneLoaded(_availableLevelList[0]))
        {
            return;
        }

        // LoadScene(_availableLevelList[0]);
        onRestartLevelEvent?.Invoke(false);
        StartCoroutine(LoadLevelWithProgressBar(_availableLevelList[0]));
    }

    public void UnloadCurrentScene()
    {
        if (!IsSceneLoaded(_currentLoadedScene))
        {
            return;
        }

        UnloadScene(_currentLoadedScene);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        // if (IsSceneLoaded(_menuList[0])) return;
        SceneManager.LoadSceneAsync(0 ,LoadSceneMode.Single);
    }

    public void UnloadMainMenu()
    {
        if (!IsSceneLoaded(_menuList[0])) return;
        UnloadScene(_menuList[0]);
    }
    #endregion

    #region LoadingScreen

    // IEnumerator LoadingScreen()
    // {
    //     float totalProgress = 0;
    //     for (int i = 0; i < scenesToLoad.Count; ++i)
    //     {
    //         totalProgress += scenesToLoad[i].progress;
    //         _loadingProgress.RuntimeValue = Mathf.Clamp01(totalProgress / 0.9f);
    //         // _onLoadingProgressEventChannel.RaiseEvent(currentLoadingProgress);
    //         // Debug.Log(currentLoadingProgress);
    //         yield return null;
    //     }
    // }

    // public async void UpdateProgressBar()
    // {
    //     float totalProgress = 0f;

    //     for (int i = 0; i < scenesToLoad.Count; ++i)
    //     {
    //         totalProgress += scenesToLoad[i].progress;
    //     }

    //     float currentLoadProgress = Mathf.Clamp01(totalProgress / 0.9f);
    //     do
    //     {
    //         await Task.Delay(100);
    //         _onLoadingProgressEventChannel.RaiseEvent(currentLoadProgress);
    //     } while (currentLoadProgress < 0.9f);
    // }
    #endregion

    
    public void PauseGame()
    {
        Time.timeScale = 0f;
        Debug.Log("Game paused!");
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1f;
        Debug.Log("Game unpaused!");
    }
}
