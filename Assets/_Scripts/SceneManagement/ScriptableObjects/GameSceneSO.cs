using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New GameScene", menuName = "Scene Management/GameScene", order = 1)]
public class GameSceneSO : ScriptableObject
{
    public enum SceneType
    {
        MainMenu,
        Level
    }
    
    [Header("General Information")]
    [Tooltip("This need to be the same as the level name or else it won't work.")]
    public string sceneName;
    public string sceneTitle;
    public string shortDescription;
    public SceneType sceneType;

    [Header("Audios")]
    public AudioClip music;

    [Range(0.0f, 1.0f)]
    public float musicVolume;

    [Header("For Level only")]
    public GameSceneSO nextLevel;
    public bool isLastLevel;
    public Transform playerSpawnLocation;
}