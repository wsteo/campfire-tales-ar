using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

public class SceneInitialization : MonoBehaviour
{

    [SerializeField] private GameSceneSO _currentSceneSO;
    [SerializeField] private Transform _camTargetPosition;
    public delegate void SceneLoadedEvent(GameSceneSO currentLevelSO);
    public static event SceneLoadedEvent onSceneLoadedEvent;

    public delegate void UpdateCamTransformEvent(Transform _camTargetPosition);
    public static event UpdateCamTransformEvent onUpdateCamTransformEvent;

    void Awake()
    {
    }

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        PingLevelForSceneInformation();
    }

    public void PingLevelForSceneInformation()
    {
        onSceneLoadedEvent?.Invoke(_currentSceneSO);
        onUpdateCamTransformEvent?.Invoke(_camTargetPosition);
        Debug.Log("Ping! Scene Loaded.");
    }
}
