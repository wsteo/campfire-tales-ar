using UnityEngine;
using DG.Tweening;

public class DOTeenRotateBehaviour : MonoBehaviour
{
    [SerializeField] private Transform _targetObject;
    [SerializeField] private float _cycleLength = 2f;
    [SerializeField] private Vector3 rotation;

    private void Start()
    {
        _targetObject.DORotate(rotation, _cycleLength * 0.5f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        // _targetObject.DOMoveY(_targetObject.transform.position.y + 1, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InBounce);
    }
}