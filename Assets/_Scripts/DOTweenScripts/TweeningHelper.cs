using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public static class TweeningHelper
{
    public static void ObjectElasticScaleIn(Transform targetTransform, float delayDuration)
    {
        CheckEaseInOut(targetTransform, delayDuration, Ease.InElastic);
    }

    public static void ObjectElasticScaleOut(Transform targetTransform, float delayDuration)
    {
        CheckEaseInOut(targetTransform, delayDuration, Ease.OutElastic);
    }

    private static void CheckEaseInOut(Transform targetTransform, float delayDuration, Ease easeEffectType)
    {
        Sequence sequence1 = DOTween.Sequence();
        if (delayDuration > 0) sequence1.AppendInterval(delayDuration);
        sequence1.Append(targetTransform.DOScale(Vector3.one, 1f).SetEase(easeEffectType));
    }
}
