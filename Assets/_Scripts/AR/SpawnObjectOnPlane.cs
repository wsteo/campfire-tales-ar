using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.InputSystem;

[RequireComponent(typeof(ARRaycastManager))]
public class SpawnObjectOnPlane : MonoBehaviour
{
    [SerializeField]
    private InputManager _inputManager;

    private ARRaycastManager _arRaycastManager;
    private GameObject _spawnedObject;

    [SerializeField]
    private GameObject _navigationBaker;

    [SerializeField]
    private GameObject placeablePrefab;

    [SerializeField]
    private Camera _cam;

    static List<ARRaycastHit> s_Hit = new List<ARRaycastHit>();

    private void Awake()
    {
        _arRaycastManager = GetComponent<ARRaycastManager>();
    }

    private void OnEnable()
    {
        _inputManager.OnStartTouch += InteractWithScreen;
    }

    private void OnDisable()
    {
        _inputManager.OnEndTouch -= InteractWithScreen;
    }

    private Vector2 touchPosition;

    private bool isTouch = false;

    Vector3 screenCoordinates;
    Vector3 worldCoordinated;

    public void InteractWithScreen(Vector2 screenPosition, float time)
    {
        // touchPosition = screenPosition;
        screenCoordinates = new Vector3(screenPosition.x, screenPosition.y, _cam.nearClipPlane);
        worldCoordinated = _cam.ScreenToWorldPoint(screenCoordinates);

        Debug.Log("Touch Detected! Coordinate: " + screenCoordinates.x + "," + screenCoordinates.y + screenCoordinates.z);
        // Debug.Log("Touch Detected! Coordinate: " + touchPosition.x + "," + touchPosition.y);
        isTouch = true;
    }

    // bool TryGetTouchPosition(out Vector2 touchPosition)
    // {
    //     if (!isTouch)
    //     {
    //         touchPosition = currentTouchPosition;
    //         isTouch = false;
    //         Debug.Log(touchPosition.x + "," + touchPosition.y);
    //         return true;
    //     }

    //     touchPosition = default;
    //     Debug.Log(touchPosition.x + "," + touchPosition.y);
    //     return false;
    // }

    private void Update()
    {
        if (isTouch)
        {
            if (_arRaycastManager.Raycast(screenCoordinates, s_Hit, TrackableType.PlaneWithinPolygon))
            {
                var hitPose = s_Hit[0].pose;
                if (_spawnedObject == null)
                {
                    // _spawnedObject = Instantiate(placeablePrefab, hitPose.position, hitPose.rotation);
                    
                    _spawnedObject = placeablePrefab;
                    _spawnedObject.transform.position = hitPose.position;
                    _spawnedObject.transform.rotation = hitPose.rotation;
                    _spawnedObject.SetActive(true);
                    _navigationBaker.SetActive(true);
                    Debug.Log("Spawn New Object");
                    // DisablePlaneManager();
                }
                else
                {
                    _spawnedObject.transform.position = hitPose.position;
                    _spawnedObject.transform.rotation = hitPose.rotation;
                    _navigationBaker.SetActive(true);
                    
                    Debug.Log("Move Object");
                }
            }
            
            isTouch = false;
        }        
    }

    // public void DisablePlaneTracking()
    // {
    //     ARPlaneManager arPlaneManager = GetComponent<ARPlaneManager>();
    //     arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = false;
    //     arPlaneManager.SetTrackablesActive(false);
    // }

    // public void EnablePlaneTracking()
    // {
    //     ARPlaneManager arPlaneManager = GetComponent<ARPlaneManager>();
    //     arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = true;
    //     arPlaneManager.SetTrackablesActive(true);
    // }
}
