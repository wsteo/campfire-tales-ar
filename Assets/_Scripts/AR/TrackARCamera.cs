using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class TrackARCamera : MonoBehaviour
{
    [SerializeField] private ARSessionOrigin _arSessionOrigin;
    [SerializeField] private Camera _arCam;
    [SerializeField] private Transform _arCamTransform;
    public delegate void RequestARCamTransformEvent(Transform arCamTransform);
    public static event RequestARCamTransformEvent onRequestARCamTransformEvent;

    void Update()
    {
        UpdateCurrentTransform();
    }

    public void UpdateCurrentTransform()
    {
        _arCamTransform = _arCam.transform;
        onRequestARCamTransformEvent?.Invoke(_arCamTransform);
    }
}
