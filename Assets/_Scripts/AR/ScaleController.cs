using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARSessionOrigin))]
public class ScaleController : MonoBehaviour
{

    [SerializeField]
    private Slider _slider;

    [SerializeField]
    private ARSessionOrigin _arSessionOrigin;

    [SerializeField]
    private float _min = 0.1f;

    [SerializeField]
    private float _max = 10f;

    float scale
    {
        get
        {
            return _arSessionOrigin.transform.localScale.x;
        }
        set
        {
            _arSessionOrigin.transform.localScale = Vector3.one * value;
        }
    }

    private void OnEnable()
    {
        if (_slider != null)
        {
            _slider.value = (scale - _min) / (_max - _min);
        }
    }

    public void OnSliderChanged()
    {
        if (_slider != null)
        {
            scale = _slider.value * (_max - _min) + _min;
        }
    }
}