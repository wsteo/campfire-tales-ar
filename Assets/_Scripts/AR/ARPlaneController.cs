using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlaneController : MonoBehaviour
{
    [SerializeField]
    private ARPlaneManager arPlaneManager;

    [SerializeField]
    private ARSession _arSession;

    [SerializeField]
    private bool _isARPlaceMeshVisualizerEnabled = true;

    private void Awake()
    {
        DisablePlaneTracking();
        arPlaneManager = GetComponent<ARPlaneManager>();
        arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = _isARPlaceMeshVisualizerEnabled;
    }

    public void DisablePlaneTracking()
    {
        // arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = false;
        arPlaneManager.SetTrackablesActive(false);
        Debug.Log("Tracking Disable");
    }

    public void EnablePlaneTracking()
    {
        // arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = true;
        arPlaneManager.SetTrackablesActive(true);
        Debug.Log("Tracking Enable");
    }

    public void ResetARSession()
    {
        _arSession.Reset();
    }
}
