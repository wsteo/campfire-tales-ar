using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using DG.Tweening;

namespace CampfireTalesAR.AugmentedRealityFunctionality
{
    public class LevelPlacementController : MonoBehaviour
    {
        // [SerializeField] private GameObject _levelPlacementIndicatorPrefab;
        [SerializeField] private GameObject _levelPlacementIndicator;
        [SerializeField] private ARRaycastManager _arRaycastManager;
        [SerializeField] private ARPlaneManager _arPlaneManager;
        [SerializeField] private ARSessionOrigin _arSessionOrigin;
        [SerializeField] private bool _isLevelPlaced;
        [SerializeField] private Transform _targetReferencePoint;
        [SerializeField] private Transform _arCam;
        [SerializeField] private Transform _fire;
        [SerializeField] private bool _hasLevelRestart;

        private void OnEnable()
        {
            SceneInitialization.onUpdateCamTransformEvent += MoveARCameraToTargetLocation;
            SceneAndLevelManager.onRestartLevelEvent += CheckIfLevelRestart;
        }

        private void OnDisable()
        {
            SceneInitialization.onUpdateCamTransformEvent += MoveARCameraToTargetLocation;
            SceneAndLevelManager.onRestartLevelEvent -= CheckIfLevelRestart;
        }

        private void CheckIfLevelRestart(bool hasRestart)
        {
            _hasLevelRestart = hasRestart;
        }

        private void MoveARCameraToTargetLocation(Transform _camTargetPosition)
        {
           
            if(_isLevelPlaced)
                return;

            _targetReferencePoint = _camTargetPosition;
            MoveARCamera();

            Debug.Log("Ping from MoveARCameraToTargetLocation");
        }

        void Start()
        {
            _levelPlacementIndicator.transform.localScale = Vector3.zero;
        }

        void Update()
        {
            if (!_isLevelPlaced)
            {
                UpdatePlacementIndicator();
            }
        }

        private List<ARRaycastHit> hits = new List<ARRaycastHit>();
        public void UpdatePlacementIndicator()
        {
            Vector2 screenPosition = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
            _arRaycastManager.Raycast(screenPosition, hits, TrackableType.PlaneWithinPolygon);
            
            Debug.Log("Raycast Hit:" + _arRaycastManager.Raycast(screenPosition, hits, TrackableType.PlaneWithinPolygon));

            if (_arRaycastManager.Raycast(screenPosition, hits, TrackableType.PlaneWithinPolygon))
            {
                EnableLevelPlacement();
            }

            if (hits.Count > 0)
            {
                _levelPlacementIndicator.transform.position = hits[0].pose.position;
                _levelPlacementIndicator.transform.rotation = hits[0].pose.rotation;
            }
        }

        public void MoveARCamera()
        {
            DisableCampfireAnimation();
            _arSessionOrigin.MakeContentAppearAt(_targetReferencePoint.transform, _levelPlacementIndicator.transform.position, Quaternion.Euler(Vector3.up));
            _isLevelPlaced = true;
        }

        public void EnableLevelPlacement()
        {
            _levelPlacementIndicator.SetActive(true);
            EnableCampfire();
        }

        public void EnableCampfire()
        {
            _levelPlacementIndicator.transform.DOScale(Vector3.one * 5, 3f).SetEase(Ease.OutElastic);
        }

        public async void DisableCampfireAnimation()
        {
            await _levelPlacementIndicator.transform.DOScale(Vector3.zero, 1f).SetEase(Ease.InElastic).AsyncWaitForCompletion();
            _levelPlacementIndicator.SetActive(false);
        }

        public void DisableCampfire()
        {
            DisableCampfireAnimation();
        }
    }
}
