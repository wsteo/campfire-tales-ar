using UnityEngine;
using UnityEngine.Events;

public class VoidEventListener : MonoBehaviour
{
    [SerializeField]
    private VoidEventChannelSO _channel = default;

    [SerializeField]
    private UnityEvent _onEventRaised;

    public UnityEvent OnEventRaised {get => _onEventRaised; set => _onEventRaised = value;}

    private void OnEnable()
    {
        if (_channel != null)
            _channel.OnEventRaised += Respond;
    }

    private void OnDisable()
    {
        if (_channel != null)
            _channel.OnEventRaised -= Respond;
    }

    private void Respond()
    {
        OnEventRaised?.Invoke();
    }
}