using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class FloatEvent: UnityEvent <float> {}

public class FloatEventListener : MonoBehaviour
{
    [SerializeField]
    private FloatEventChannelSO _channel = default;

    [SerializeField]
    private FloatEvent _onEventRaised;

    public FloatEvent OnEventRaised {get => _onEventRaised; set => _onEventRaised = value;}

    private void OnEnable()
    {
        if (_channel != null)
            _channel.OnEventRaised += Respond;
    }

    private void OnDisable()
    {
        if (_channel != null)
            _channel.OnEventRaised -= Respond;
    }

    private void Respond(float value)
    {
        OnEventRaised?.Invoke(value);
    }
}