using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class IntEvent: UnityEvent <int> {}

public class IntEventListener : MonoBehaviour
{
    [SerializeField]
    private IntEventChannelSO _channel = default;

    [SerializeField]
    private IntEvent _onEventRaised;

    public IntEvent OnEventRaised {get => _onEventRaised; set => _onEventRaised = value;}

    private void OnEnable()
    {
        if (_channel != null)
            _channel.OnEventRaised += Respond;
    }

    private void OnDisable()
    {
        if (_channel != null)
            _channel.OnEventRaised -= Respond;
    }

    private void Respond(int value)
    {
        OnEventRaised?.Invoke(value);
    }
}