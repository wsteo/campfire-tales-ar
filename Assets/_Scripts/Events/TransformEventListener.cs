using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TransformEvent: UnityEvent <Transform> {}

public class TransformEventListener : MonoBehaviour
{
    [SerializeField]
    private TransformEventChannelSO _channel = default;

    [SerializeField]
    private TransformEvent _onEventRaised;

    public TransformEvent OnEventRaised {get => _onEventRaised; set => _onEventRaised = value;}

    private void OnEnable()
    {
        if (_channel != null)
            _channel.OnEventRaised += Respond;
    }

    private void OnDisable()
    {
        if (_channel != null)
            _channel.OnEventRaised -= Respond;
    }

    private void Respond(Transform value)
    {
        OnEventRaised?.Invoke(value);
    }
}