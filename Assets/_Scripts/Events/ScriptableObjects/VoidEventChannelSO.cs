using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "VoidEventChannelSO", menuName = "Event Channels/VoidEventChannelSO", order = 0)]
public class VoidEventChannelSO : ScriptableObject
{
    [SerializeField]
    private UnityAction _onEventRaised;
    public UnityAction OnEventRaised { get => _onEventRaised; set => _onEventRaised = value; }

    public void RaiseEvent()
    {
        OnEventRaised?.Invoke();
    }
}