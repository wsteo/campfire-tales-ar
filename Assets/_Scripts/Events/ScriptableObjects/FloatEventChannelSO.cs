using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "FloatEventChannelSO", menuName = "Event Channels/FloatEventChannelSO", order = 1)]
public class FloatEventChannelSO : ScriptableObject
{
    [SerializeField]
    private UnityAction<float> _onEventRaised;
    public UnityAction<float> OnEventRaised { get => _onEventRaised; set => _onEventRaised = value; }

    public void RaiseEvent(float value)
    {
        OnEventRaised?.Invoke(value);
    }

}