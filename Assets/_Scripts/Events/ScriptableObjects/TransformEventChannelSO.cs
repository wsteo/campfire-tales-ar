using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "TransformEventChannelSO", menuName = "Event Channels/TransformEventChannelSO", order = 4)]
public class TransformEventChannelSO : ScriptableObject
{
    [SerializeField]
    private UnityAction<Transform> _onEventRaised;
    public UnityAction<Transform> OnEventRaised { get => _onEventRaised; set => _onEventRaised = value; }

    public void RaiseEvent(Transform value)
    {
        OnEventRaised?.Invoke(value);
    }

}