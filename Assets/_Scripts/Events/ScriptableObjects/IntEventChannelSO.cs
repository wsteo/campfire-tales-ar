using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "IntEventChannelSO", menuName = "Event Channels/IntEventChannelSO", order = 2)]
public class IntEventChannelSO : ScriptableObject
{
    [SerializeField]
    private UnityAction<int> _onEventRaised;
    public UnityAction<int> OnEventRaised { get => _onEventRaised; set => _onEventRaised = value; }

    public void RaiseEvent(int value)
    {
        OnEventRaised?.Invoke(value);
    }

}