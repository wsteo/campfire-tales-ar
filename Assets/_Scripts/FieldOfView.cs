using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.FOV
{
    public class FieldOfView : MonoBehaviour
    {
        [SerializeField]
        private float _viewRadius;
        public float ViewRadius { get => _viewRadius; set => _viewRadius = value; }

        [SerializeField]
        [Range(0, 360)]
        private float _viewAngle;
        public float ViewAngle { get => _viewAngle; set => _viewAngle = value; }

        [SerializeField]
        private LayerMask _targetMask, _obstacleMask;

        public LayerMask TargetMask { get => _targetMask; set => _targetMask = value; }
        public LayerMask ObstacleMask { get => _obstacleMask; set => _obstacleMask = value; }

        public Transform targetLocationEditorOnly;

        public bool FindVisibleTarget(out Transform target)
        {
            Collider[] targetInViewRadius = Physics.OverlapSphere(transform.position, _viewRadius, _targetMask);

            for (int i = 0; i < targetInViewRadius.Length; i++)
            {
                target = targetInViewRadius[i].transform;
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, dirToTarget) < ViewAngle / 2)
                {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);

                    if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, _obstacleMask))
                    {
                        targetLocationEditorOnly = target;
                        return true;
                    }
                }
            }

            targetLocationEditorOnly = null;
            target = null;
            return false;
        }

        public float GetTargetDistance(out Transform target)
        {
            Collider[] targetInViewRadius = Physics.OverlapSphere(transform.position, _viewRadius, _targetMask);

            for (int i = 0; i < targetInViewRadius.Length; i++)
            {
                target = targetInViewRadius[i].transform;
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, dirToTarget) < ViewAngle / 2)
                {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);

                    if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, _obstacleMask))
                    {
                        return dstToTarget;
                    }
                }
            }

            target = null;
            return -1f;
        }

        public Vector3 DirFromAngle(float angleInDegrees, bool anglesIsGlobal)
        {
            if (!anglesIsGlobal)
            {
                angleInDegrees += transform.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }
    }

}