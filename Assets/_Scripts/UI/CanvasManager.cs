using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public enum CanvasType
{
    NULL,
    MAIN_MENU,
    AR_LEVEL_PLACEMENT,
    LOADING_SCREEN,
    SETTINGS,
    IN_GAME_UI,
    WIN_LOSE_UI,
    THANKS_FOR_PLAYING_UI
}

public class CanvasManager : MonoBehaviour
{
    List<CanvasController> canvasControllerList;
    CanvasController lastActiveCanvas;

    [SerializeField] private CanvasType _previousActiveCanvas;
    public CanvasType PreviousActiveCanvas => _previousActiveCanvas;

    private void Awake()
    {
        canvasControllerList = GetComponentsInChildren<CanvasController>().ToList();
        canvasControllerList.ForEach(x => x.gameObject.SetActive(false));
        SwitchCanvas(CanvasType.MAIN_MENU);
    }

    public void SwitchCanvas(CanvasType type)
    {
        if (lastActiveCanvas != null)
            lastActiveCanvas.gameObject.SetActive(false);

        CanvasController targetCanvas = canvasControllerList.Find(x => x.CanvasType == type);
        if (targetCanvas != null)
        {
            targetCanvas.gameObject.SetActive(true);
            _previousActiveCanvas = type;
            lastActiveCanvas = targetCanvas;
        }
        else
        {
            Debug.LogWarning("The main menu canvas is not found.");
        }
    }
}