using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFaceCamera : MonoBehaviour
{
    [SerializeField]
    private Transform cam;

    private void LateUpdate()
    {
        transform.LookAt(transform.position + cam.forward);
    }

    public void UpdateCamera(Transform camTransform)
    {
        cam = camTransform;
    }
}
