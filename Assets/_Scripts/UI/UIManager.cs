using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace CampfireTalesAR.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private TMP_Text _numberOfCoins;
        public TMP_Text CurrrentScoreText { get => _numberOfCoins; set => _numberOfCoins = value; }

        [SerializeField] private TMP_Text _numOfKeyText;
        public TMP_Text NumOfKeyText { get => _numOfKeyText; set => _numOfKeyText = value; }

        [SerializeField] private GameObject _inGameUI;
        [SerializeField] private GameObject _winLoseUI;
        [SerializeField] private TMP_Text _winText;
        [SerializeField] private TMP_Text _loseText;
        [SerializeField] private TMP_Text _totalCoinCollectedHeaderText;
        [SerializeField] private TMP_Text _totalCoinCollectedText;
        [SerializeField] private GameObject _nextLevelButton;
        [SerializeField] private GameObject _thankYouCanvas;

        [SerializeField] private GameSceneSO _currentLevelLoaded;

        private int totalCoinCollected;
        private void OnEnable()
        {
            KeyManager.onUpdateKeyManagerEvent += UpdateNumOfKey;
            ScoreManager.onRequestScoreManagerEvent += UpdateTotalNumOfCoin;
            ExitPoint.onCompleteLevelEvent += ActiveUIForWinLost;
            GameManager.onGameLoseEvent += ActiveUIForWinLost;
            SceneInitialization.onSceneLoadedEvent += ActivateInGameUI;
        }

        private void OnDisable()
        {
            KeyManager.onUpdateKeyManagerEvent -= UpdateNumOfKey;
            ScoreManager.onRequestScoreManagerEvent -= UpdateTotalNumOfCoin;
            ExitPoint.onCompleteLevelEvent -= ActiveUIForWinLost;
            GameManager.onGameLoseEvent -= ActiveUIForWinLost;
            SceneInitialization.onSceneLoadedEvent -= ActivateInGameUI;
        }

        private void ActivateInGameUI(GameSceneSO currentLevelSO)
        {
            _inGameUI.SetActive(true);
            _currentLevelLoaded = currentLevelSO;
        }

        private void ActiveUIForWinLost(bool hasWin)
        {            

            _inGameUI.SetActive(false);
            _winLoseUI.SetActive(true);
            _loseText.enabled = false;
            _winText.enabled = false;


            if (!hasWin)
            {
                ActivateLoseUI();
                return;
            }
            
            if (ActivateThankYouUI())
            {
                return;
            }
            ActivateWinUI();
        }

        private void ActivateWinUI()
        {
            _winText.enabled = true;
            _totalCoinCollectedText.enabled = true;
            _totalCoinCollectedText.text = totalCoinCollected.ToString();
            _totalCoinCollectedHeaderText.enabled = true;
            _nextLevelButton.SetActive(true);
        }

        private void ActivateLoseUI()
        {
            _loseText.enabled = true;
            _totalCoinCollectedHeaderText.enabled = false;
            _totalCoinCollectedText.enabled = false;
            _nextLevelButton.SetActive(false);
        }

        private bool ActivateThankYouUI()
        {
            if (_currentLevelLoaded.isLastLevel)
            {
                _winLoseUI.SetActive(false);
                _thankYouCanvas.SetActive(true);
                return true;
            }
            return false;   
        }

        private void UpdateNumOfKey(int numberOfKeys)
        {
            _numOfKeyText.text = numberOfKeys.ToString();
        }

        private void UpdateTotalNumOfCoin(int numberOfCoins)
        {
            _numberOfCoins.text = numberOfCoins.ToString();
            totalCoinCollected = numberOfCoins;
        }
    }

}
