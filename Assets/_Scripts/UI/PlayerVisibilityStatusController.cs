using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerVisibilityStatusController : MonoBehaviour
{
    [SerializeField]
    private Sprite alertIcon, hiddenIcon, visibleIcon;

    [SerializeField]
    private Image playerVisibilityIconUI;

    public void UpdateVisibilityUIToAlert()
    {
        playerVisibilityIconUI.sprite = alertIcon;
        playerVisibilityIconUI.color = Color.red;
    }

    public void UpdateVisibilityUIToHidden()
    {
        playerVisibilityIconUI.sprite = hiddenIcon;
        playerVisibilityIconUI.color = Color.white;
    }

    public void UpdateVisibilityUIToVisible()
    {
        playerVisibilityIconUI.sprite = visibleIcon;
        playerVisibilityIconUI.color = Color.white;
    }
}
