using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class CanvasSwitcher : MonoBehaviour
{
    [SerializeField] private CanvasType _desiredCanvasType;

    [SerializeField] private CanvasManager _canvasManager;

    Button menuButton;

    // Start is called before the first frame update
    void Start()
    {
        menuButton = GetComponent<Button>();
        menuButton.onClick.AddListener(OnButtonClicked);
        _canvasManager = GameObject.Find("UIGroup").GetComponent<CanvasManager>();
    }

    private void OnButtonClicked()
    {
        if (_canvasManager.PreviousActiveCanvas != CanvasType.NULL)
        {
            if (_desiredCanvasType == CanvasType.NULL)
            {
                _canvasManager.SwitchCanvas(_canvasManager.PreviousActiveCanvas);
            }
        }
        
        _canvasManager.SwitchCanvas(_desiredCanvasType);
    }
}
