using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New BoolVariable", menuName = "ScriptableObjectVariables/BoolVariable", order = 1)]
public class BoolVariable : BaseVariable<bool> { }
