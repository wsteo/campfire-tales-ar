using UnityEngine;
using System;

[CreateAssetMenu(fileName = "TransformVariable", menuName = "ScriptableObjectVariables/TransformVariable", order = 0)]
public class TransformVariable : BaseVariable<Transform> {}