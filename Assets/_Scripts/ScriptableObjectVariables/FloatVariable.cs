using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New FloatVariable", menuName = "ScriptableObjectVariables/FloatVariable", order = 2)]
public class FloatVariable : BaseVariable<float> { }
