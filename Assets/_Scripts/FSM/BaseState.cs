using System;
using UnityEngine;
using System.Collections;

namespace FSM
{
    public abstract class BaseState
    {
        protected bool _isStateActive;
        
        public virtual void Enter()
        {
            _isStateActive = true;
        }
        public abstract void LogicUpdate();
        public abstract void PhysicsUpdate();
        public virtual void Exit()
        {
            _isStateActive = false;
        }
    }
}