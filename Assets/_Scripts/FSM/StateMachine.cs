using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    public abstract class StateMachine<T> : MonoBehaviour where T : BaseState
    {
        private T _currentState;
        public T CurrentState => _currentState;
        public void Initialize(T startingState)
        {
            _currentState = startingState;
            startingState.Enter();
        }

        public void ChangeState(T newState)
        {
            _currentState.Exit();

            _currentState = newState;
            newState.Enter();
        }
    }

}
