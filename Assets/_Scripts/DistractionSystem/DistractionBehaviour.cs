using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using EnemyAISystem;

public class DistractionBehaviour : MonoBehaviour, I_Interactable
{
    public delegate void SendSignal(GameObject distractionPost);
    public static event SendSignal SendSignalEvent;

    [SerializeField]
    private SO_InteractionInfo _distractionDeviceInfo;

    public SO_InteractionInfo DistractionDeviceInfo => _distractionDeviceInfo;

    [SerializeField]
    private bool isDistractionActive = false;

    [SerializeField]
    private List<EnemyAIController> _enemyList;

    [SerializeField]
    private Material signalColor;

    [SerializeField]
    private List<GameObject> _enemyInDetectionRange;

    // [SerializeField]
    // private GameObject signalLight;

    // [SerializeField]
    // private Material signalClear;

    private void Start()
    {
        signalColor.color = Color.green;
    }

    // private void OnEnable()
    // {
    //     // InteractionManager.InInteractionRangeEvent += InteractionAction;
    //     EnemyInvestigateState.OnDisableDistractionEvent += EndDistraction;
    // }

    // private void OnDisable()
    // {
    //     // InteractionManager.InInteractionRangeEvent -= InteractionAction;
    //     EnemyInvestigateState.OnDisableDistractionEvent -= EndDistraction;
    // }

    public void ActivateDistraction()
    {
        if (!isDistractionActive)
        {
            Debug.Log("Start Distraction!");

            isDistractionActive = true;
            signalColor.color = Color.red;
            // SendSignalEvent?.Invoke(this.gameObject);
            foreach (GameObject enemy in _enemyInDetectionRange)
            {
                enemy.GetComponent<EnemyAIController>().IsDistracted = true;
                enemy.GetComponent<EnemyAIController>().distractionPostPoint = this.gameObject.transform;
            }
        }
    }

    public void DeactivateDistraction()
    {
        isDistractionActive = false;
        signalColor.color = Color.green;
        // StopCoroutine(SignalFrequency());
    }

    IEnumerator SignalFrequency()
    {
        while (isDistractionActive)
        {
            float frequency = 5f;
            yield return new WaitForSeconds(frequency);

            Debug.Log("Invoke Send Signal Event!");
            SendSignalEvent?.Invoke(this.gameObject);
        }
    }

    // public override void InteractionAction(GameObject interactableObject)
    // {
    //     SO_InteractionInfo interactionInfo = interactableObject.GetComponent<DistractionBehaviour>().DistractionDeviceInfo;
        
    //     if (interactionInfo.ObjectInteractionType == SO_InteractionInfo.InteractionType.Distraction && !isDistractionActive)
    //     {
    //         ActivateDistraction(interactableObject);
    //     }
    //     else
    //     {
    //         EndDistraction();
    //     }
    // }

    // private void OnCollisionEnter(Collision other)
    // {
    //     if (other.gameObject.tag == "Enemy")
    //     {
    //         _enemyList.Add(other.gameObject.GetComponent<EnemyAIController>());
    //     }
    // }

    public void Interact()
    {
        if (!isDistractionActive)
            ActivateDistraction();
        else if (isDistractionActive)
            DeactivateDistraction();
    }
}