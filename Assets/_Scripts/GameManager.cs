using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    [SerializeField] private bool _isPlayerDead;

    [SerializeField] private bool _isPlayerVisible;

    [SerializeField] private int _numOfKey;

    [SerializeField] private int _numOfCoins;

    [SerializeField] private bool _isGamePause;

    public delegate void GameLoseEvent(bool hasWin);
    public static event GameLoseEvent onGameLoseEvent;

    private void OnEnable()
    {
        SeaBehaviour.onPlayerFallIntoWater += PlayerLose;
        EnemyCollision.onCaughtPlayerEvent += PlayerLose;
        KeyManager.onUpdateKeyManagerEvent += UpdateNumberOfKey;
    }

    private void OnDisable()
    {
        SeaBehaviour.onPlayerFallIntoWater -= PlayerLose;
        EnemyCollision.onCaughtPlayerEvent -= PlayerLose;
        KeyManager.onUpdateKeyManagerEvent -= UpdateNumberOfKey;
    }

    private void UpdateNumberOfKey(int numberOfKeys)
    {
        _numOfKey = numberOfKeys;
    }

    private void PlayerLose()
    {
        _isPlayerDead = true;
        PlayerLoseAnimation();
    }

    async void PlayerLoseAnimation()
    {
        await _player.GetComponent<Transform>().DOScale(Vector3.zero, 0.5f).SetEase(Ease.InOutBounce).AsyncWaitForCompletion();
        _player.SetActive(false);
        onGameLoseEvent?.Invoke(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        _player.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

}
