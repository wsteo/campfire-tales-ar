using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerThrowCoin : MonoBehaviour
{
    public delegate void CoinDistractionEvent(Vector3 pos);
    public static event CoinDistractionEvent OnCoinDistractionEvent;
    
    [SerializeField]
    private PlayerMovements playerInput;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private CharacterController controller;

    [SerializeField]
    private GameObject m_coinPrefab;

    private GameObject _instantiatedCoin;

    [SerializeField]
    private Vector3 coinDistance;

    [SerializeField]
    private float forceMagnitude = 1.0f;

    [SerializeField]
    private bool _hasTossedCoin = false;

    private void Update()
    {
        if (playerInput.inputManager.Inputs.CharacterControls.ThrowCoin.triggered)
        {
            ThrowCoin(player.transform.position + coinDistance);
        }
    }

    private void ThrowCoin(Vector3 pos)
    {
        _instantiatedCoin = Instantiate(m_coinPrefab, pos, Quaternion.identity);

        if (OnCoinDistractionEvent != null)
        {
            OnCoinDistractionEvent?.Invoke(_instantiatedCoin.transform.position);
            Debug.Log("Coin Tossed at " + _instantiatedCoin.transform.position);
        }
    }
}
