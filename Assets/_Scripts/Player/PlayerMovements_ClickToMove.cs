using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;

public class PlayerMovements_ClickToMove : MonoBehaviour
{
    [SerializeField]
    private Camera _cam;

    [SerializeField]
    private NavMeshAgent _player;

    [SerializeField]
    private Animator _playerAnimator;

    [SerializeField]
    private GameObject _targetDestination;

    [SerializeField]
    private InputManager _inputManager;

    private Vector2 touchPosition;

    private bool isTouch = false;

    private void OnEnable()
    {
        _inputManager.OnStartTouch += Move;
        _targetDestination = Instantiate(_targetDestination);
        _targetDestination.SetActive(false);
    }

    private void OnDisable()
    {
        _inputManager.OnEndTouch -= Move;
    }

    Ray ray;
    RaycastHit hitPoint;

    Vector3 screenCoordinates;
    Vector3 worldCoordinated;
    public void Move(Vector2 screenPosition, float time)
    {
        screenCoordinates = new Vector3(screenPosition.x, screenPosition.y, _cam.nearClipPlane);
        worldCoordinated = _cam.ScreenToWorldPoint(screenCoordinates);

        Debug.Log("Touch Detected! Coordinate: " + screenCoordinates.x + "," + screenCoordinates.y + screenCoordinates.z);
        isTouch = true;

    }

    void Update()
    {
        ray = _cam.ScreenPointToRay(screenCoordinates);

        if (isTouch)
        {
            if (Physics.Raycast(ray, out hitPoint) && hitPoint.collider.CompareTag("ClickableArea"))
            {
                _targetDestination.transform.position = hitPoint.point;
                _player.SetDestination(hitPoint.point);
            }
            isTouch = false;
        }

        if (_player.velocity != Vector3.zero)
        {
            _playerAnimator.SetBool("isWalking",true);
            _targetDestination.SetActive(true);
        }
        else if (_player.velocity == Vector3.zero)
        {
            _playerAnimator.SetBool("isWalking", false);
            _targetDestination.SetActive(false);
        }
    }
}
