using UnityEngine;

public class PlayerFootStep : MonoBehaviour
{
    public delegate void CharacterWalkEvent();
    public static event CharacterWalkEvent onCharacterWalkEvent;
    private void Step()
    {
        Debug.Log("STEP!");
        onCharacterWalkEvent?.Invoke();
    }
}