using System;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField]
    private InputManager _inputManager;

    [SerializeField]
    private InteractionRangeChecker _interactionRangeChecker;

    public delegate void InteractionWithObject();
    public static event InteractionWithObject OnInteractionWithObject;
    private bool _inInteractionRange;

    private void OnEnable()
    {
        // InteractionRangeChecker.InInteractionRangeEvent += CheckInteractionRange;
    }

    private void OnDisable()
    {
        // InteractionRangeChecker.InInteractionRangeEvent -= CheckInteractionRange;
    }

    private void Awake()
    {
        _interactionRangeChecker = GetComponent<InteractionRangeChecker>();
    }

    private void Update()
    {
        if (_inputManager.Inputs.CharacterControls.Interact.triggered && _interactionRangeChecker.InteractableObject != null)
        {
            InteractionWithTarget();
        }
    }

    public void InteractionWithTarget()
    {
        if (_interactionRangeChecker.InteractableObject != null)
        {
            var interactable = _interactionRangeChecker.InteractableObject.GetComponent<I_Interactable>();
            if (interactable == null) return;
            interactable.Interact();
        }
    }
}