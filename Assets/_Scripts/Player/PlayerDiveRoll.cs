using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerDiveRoll : MonoBehaviour
{
    public InputManager inputManager;
    [SerializeField]
    private CharacterController _controller;
    public CharacterController Controller { get => _controller; }
    // Dive Roll Function (Need to rework)
    [Header("Dive Roll")]
    [SerializeField]
    private float _dashTime = 0.1f;

    [SerializeField]
    private float _dashSpeed = 100f;

    [SerializeField]
    private bool _hasDived = false;

    [SerializeField]
    [Range(0, 3)]
    private int _diveRollCharge = 3;

    private Coroutine _diveRoll, _diveRollCooldown;

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
    }

    private void DiveRoll()
    {
        if (_diveRoll != null)
            StopCoroutine(_diveRoll);

        if (_diveRollCooldown != null)
            StopCoroutine(_diveRollCooldown);

        if (inputManager.Inputs.CharacterControls.DiveRoll.triggered && !_hasDived && _diveRollCharge != 0)
        {
            if (_diveRollCharge > 0 && _diveRollCharge <= 3)
                _diveRollCharge -= 1;

            _diveRoll = StartCoroutine(DiveRollCoroutine());
        }
        else if (_hasDived)
        {
            StartCoroutine(DiveRollCooldownCoroutine());
        }
    }

    private IEnumerator DiveRollCoroutine()
    {
        float startTime = Time.time;
        while (Time.time < startTime + _dashTime)
        {
            _hasDived = true;
            Controller.Move(transform.forward * _dashSpeed * Time.deltaTime);
            yield return null;
        }
        _diveRoll = null;
    }

    private IEnumerator DiveRollCooldownCoroutine()
    {
        yield return new WaitForSeconds(3f);

        if (_hasDived && _diveRollCharge <= 3 && _diveRollCharge >= 0)
        {
            _hasDived = false;
        }
        _diveRollCooldown = null;
    }
}