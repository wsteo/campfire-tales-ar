using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovements : MonoBehaviour
{
    public InputManager inputManager;

    // public PlayerInputs input;

    [SerializeField]
    private CharacterController _controller;
    public CharacterController Controller { get => _controller; }
    private Vector3 playerVelocity;
    private bool groundedPlayer;

    [SerializeField]
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float jumpHeight = 1.0f;
    [SerializeField]
    private float gravityValue = -9.81f;

    [SerializeField]
    private float _camPivot;

    [SerializeField]
    private Transform _cam;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private Rigidbody _rb;

    public delegate void PlayerMovesEvent();
    public static event PlayerMovesEvent onPlayerMovesEvent;

    private void OnEnable()
    {
        TrackARCamera.onRequestARCamTransformEvent += GetARCamTransform;
    }

    private void OnDisable()
    {
        TrackARCamera.onRequestARCamTransformEvent -= GetARCamTransform;
    }

    private void GetARCamTransform(Transform arCamTransform)
    {
        _cam = arCamTransform;
    }

    private void Update()
    {
        groundedPlayer = _controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector2 movementInput = inputManager.Inputs.CharacterControls.Movements.ReadValue<Vector2>();

        Vector3 camF = _cam.forward;
        Vector3 camR = _cam.right;

        camF.y = 0;
        camF.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;

        Vector3 move = new Vector3(movementInput.x, 0f, movementInput.y);
        _controller.Move((camF * movementInput.y + camR * movementInput.x) * Time.deltaTime * 5);

        // Player Look At
        Vector3 direction = _rb.velocity;
        direction.y = 0f;

        if (inputManager.Inputs.CharacterControls.Movements.ReadValue<Vector2>().sqrMagnitude > 0.1f && direction.sqrMagnitude > 0.1f)
        {
            this._rb.rotation = Quaternion.LookRotation(direction, Vector3.up);
        }
        else
        {
            _rb.angularVelocity = Vector3.zero;
        }

        // Animation
        if (move != Vector3.zero)
        {
            onPlayerMovesEvent?.Invoke();
            _animator.SetBool("isWalking", true);
        }
        else if (move == Vector3.zero)
        {
            _animator.SetBool("isWalking", false);
        }

        if (inputManager.Inputs.CharacterControls.Jump.triggered && groundedPlayer)
        {
            Jump();
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        _controller.Move(playerVelocity * Time.deltaTime);
    }

    public void Jump()
    {
        playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
    }
}
