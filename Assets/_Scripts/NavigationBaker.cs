using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.AI.Navigation;

public class NavigationBaker : MonoBehaviour
{

    public NavMeshSurface[] surfaces;

    // Use this for initialization
    void OnEnable()
    {
        SceneInitialization.onSceneLoadedEvent += InitializedNavMesh;
        DoorBehaviour.onOpenDoorEvent += RefreshNavMesh;
        RefreshNavMesh();
    }

    private void OnDisable()
    {
        SceneInitialization.onSceneLoadedEvent += InitializedNavMesh;
        DoorBehaviour.onOpenDoorEvent += RefreshNavMesh;
        RefreshNavMesh();
    }

    public void InitializedNavMesh(GameSceneSO LevelSO)
    {
        if (LevelSO.sceneType != GameSceneSO.SceneType.Level)
            return;
        else
            RefreshNavMesh();
    }

    public void RefreshNavMesh()
    {
        for (int i = 0; i < surfaces.Length; i++)
        {
            surfaces[i].BuildNavMesh();
            Debug.Log("Baking Nav Mesh!");
        }
    }

}
