using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Gameplay.FOV
{
    [CustomEditor(typeof(FieldOfView))]
    public class FieldOfViewEditor : Editor
    {
        void OnSceneGUI()
        {
            FieldOfView fov = (FieldOfView)target;
            
            // Inner circle
            Handles.color = Color.white;
            Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.ViewRadius);

            Vector3 viewAngleA = fov.DirFromAngle(-fov.ViewAngle / 2, false);
            Vector3 viewAngleB = fov.DirFromAngle(fov.ViewAngle / 2, false);
    
            Handles.color = Color.white;
            Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleA * fov.ViewRadius);
            Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleB * fov.ViewRadius);

            Handles.color = Color.green;
            if (fov.FindVisibleTarget(out Transform visibleTarget))
            {
                Handles.DrawLine(fov.transform.position, visibleTarget.position);
            }
        }
    }

}