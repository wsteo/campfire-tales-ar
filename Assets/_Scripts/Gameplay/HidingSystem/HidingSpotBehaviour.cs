using UnityEngine;

public class HidingSpotBehaviour : MonoBehaviour
{
    [SerializeField]
    private VoidEventChannelSO _onPlayerEnterHidingSpot;

    [SerializeField]
    private VoidEventChannelSO _onPlayerExitHidingSpot;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _onPlayerEnterHidingSpot.RaiseEvent();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _onPlayerExitHidingSpot.RaiseEvent();
        }
    }
}