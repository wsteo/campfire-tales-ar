using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaBehaviour : MonoBehaviour
{
    public delegate void PlayerFallIntoWaterEvent();

    public static event PlayerFallIntoWaterEvent onPlayerFallIntoWater;

    // Start is called before the first frame update
    void Start() { }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            onPlayerFallIntoWater?.Invoke();
        }
    }
}
