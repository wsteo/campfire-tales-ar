using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExitPoint : MonoBehaviour
{
    [SerializeField]
    private bool isWin = false;

    public delegate void CompleteLevelEvent(bool hasWin);
    public static event CompleteLevelEvent onCompleteLevelEvent;

    public delegate void PlayerWinEvent();

    public static event PlayerWinEvent onPlayerWinEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !isWin)
        {
            isWin = true;
            onCompleteLevelEvent?.Invoke(true);
            onPlayerWinEvent?.Invoke();
        }
    }
}
