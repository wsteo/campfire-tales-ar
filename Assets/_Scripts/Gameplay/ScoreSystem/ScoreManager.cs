using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private int _totalNumOfCoinsCollected;

    public delegate void RequestScoreManagerEvent(int totalNumOfCoinsCollected);

    public static event RequestScoreManagerEvent onRequestScoreManagerEvent;

    private void OnEnable()
    {
        CoinBehaviour.onCoinCollectedEvent += UpdateCoin;
    }

    private void OnDisable()
    {
        CoinBehaviour.onCoinCollectedEvent -= UpdateCoin;
    }

    private void Awake()
    {
        onRequestScoreManagerEvent?.Invoke(0);
    }

    private void UpdateCoin()
    {
        _totalNumOfCoinsCollected = _totalNumOfCoinsCollected + 1;
        onRequestScoreManagerEvent?.Invoke(_totalNumOfCoinsCollected);
        Debug.Log("Hello from score manager.");
    }
}
