using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CoinBehaviour : MonoBehaviour
{
    public delegate void CoinCollectedEvent();
    public static event CoinCollectedEvent onCoinCollectedEvent;
    [SerializeField] private float _cycleLength = 2f;

    private void Start() 
    {
        // transform.DORotate(new Vector3(0,360,0), _cycleLength * 0.5f).SetLoops(-1, LoopType.Restart);
    }

    private void Update() { }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            onCoinCollectedEvent?.Invoke();
            Debug.Log("Player pick up coin!");
            this.gameObject.SetActive(false);
        }
    }
}
