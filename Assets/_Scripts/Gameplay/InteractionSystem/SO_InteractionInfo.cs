using UnityEngine;

[CreateAssetMenu(fileName = "SO_InteractionInfo", menuName = "CampfireTalesAR-Prototype/SO_InteractionInfo", order = 0)]
public class SO_InteractionInfo : ScriptableObject
{
    public enum InteractionType
    {
        Distraction,
        Door
    }

    [SerializeField]
    private InteractionType _objectInteractionType;
    public InteractionType ObjectInteractionType => _objectInteractionType;

    [SerializeField]
    private string _interactionName;
    public string InteractionName => _interactionName;

    [SerializeField]
    private Sprite _interactionIcon;
    public Sprite InteractionIcon => _interactionIcon;
}