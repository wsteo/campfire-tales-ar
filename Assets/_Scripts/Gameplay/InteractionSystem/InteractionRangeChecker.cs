using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractionRangeChecker : MonoBehaviour
{
    // public delegate void InInteractionRange(GameObject interactableObject);
    // public static event InInteractionRange InInteractionRangeEvent;

    [SerializeField]
    private bool isInteractable;

    [SerializeField]
    private GameObject _interactableObject;
    public GameObject InteractableObject => _interactableObject;
    private bool isInteractionButton;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Interactable")
        {
            isInteractable = true;
            _interactableObject = other.gameObject;
            // InInteractionRangeEvent?.Invoke(_interactableObject);
            Debug.Log("Enter interaction range");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Interactable")
        {
            isInteractable = false;
            _interactableObject = null;
            // InInteractionRangeEvent?.Invoke(_interactableObject);
            Debug.Log("Exit interaction range");
        }
    }
}