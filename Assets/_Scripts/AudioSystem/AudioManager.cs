using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private GameSceneSO _currentLevel;
    [SerializeField] private AudioClip _pickUpCoinSFX;
    [SerializeField] private AudioClip _pickUpKeySFX;
    [SerializeField] private AudioClip _unlockDoorSFX;
    [SerializeField] private AudioClip _buttonPressedSFX;

    [SerializeField] private AudioClip[] _walkingSFXList;

    [Range(0f,1f)]
    public float sfxVolume = 1f;

    private void OnEnable()
    {
        SceneInitialization.onSceneLoadedEvent += PlayMusic;
        CoinBehaviour.onCoinCollectedEvent += PlayCoinSoundSFX;
        DoorBehaviour.onOpenDoorEvent += PlayUnlockDoorSFX;
        KeyBehaviour.OnKeyPickUp += PlayPickUpKeySFX;
        PlayerFootStep.onCharacterWalkEvent += PlayWalkingSFX;
    }

    private void OnDisable()
    {
        SceneInitialization.onSceneLoadedEvent -= PlayMusic;
        CoinBehaviour.onCoinCollectedEvent -= PlayCoinSoundSFX;
        DoorBehaviour.onOpenDoorEvent -= PlayUnlockDoorSFX;
        KeyBehaviour.OnKeyPickUp -= PlayPickUpKeySFX;
        PlayerFootStep.onCharacterWalkEvent -= PlayWalkingSFX;
    }

    public void PlayMusic(GameSceneSO currentLevelSO)
    {
        _currentLevel = currentLevelSO;
        _audioSource.clip = _currentLevel.music;
        _audioSource.volume = _currentLevel.musicVolume;
        _audioSource.Play();
    }

    public void UpdateSFXVolume(float currentVolume)
    {
        sfxVolume = currentVolume;
    }

    private void PlayCoinSoundSFX()
    {
        _audioSource.PlayOneShot(_pickUpCoinSFX, sfxVolume);
    }

    public void PlayButtonSoundFX()
    {
        _audioSource.PlayOneShot(_buttonPressedSFX, sfxVolume);
    }

    public void PlayPickUpKeySFX(SO_InteractionInfo _interactionInfo)
    {
        _audioSource.PlayOneShot(_pickUpKeySFX, sfxVolume);
    }

    public void PlayUnlockDoorSFX()
    {
        _audioSource.PlayOneShot(_unlockDoorSFX, sfxVolume);
    }

    public void PlayWalkingSFX()
    {
        AudioClip walkingSFX = GetRandomWalkingSFX();
        _audioSource.PlayOneShot(walkingSFX, sfxVolume);
    }

    private AudioClip GetRandomWalkingSFX()
    {
        return _walkingSFXList[UnityEngine.Random.Range(0, _walkingSFXList.Length)];
    }
}
