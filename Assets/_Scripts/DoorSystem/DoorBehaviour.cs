using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorBehaviour : MonoBehaviour, I_Interactable
{
        
    [SerializeField] private SO_InteractionInfo doorInfo;
    public SO_InteractionInfo DoorInfo => doorInfo;

    [SerializeField] private KeyManager _keyManager;

    public delegate void OpenDoorEvent();

    public static event OpenDoorEvent onOpenDoorEvent;

    private void Awake()
    {
        _keyManager = GameObject.Find("KeyManager").GetComponent<KeyManager>();
    }

    public void Interact()
    {
        OpenDoor();
    }

    public void OpenDoor()
    {
        if (_keyManager.SearchKeyInInventory(this.gameObject))
        {
            this.gameObject.transform.DOScale(Vector3.zero,0.5f).SetEase(Ease.InBounce);

            onOpenDoorEvent?.Invoke();
        }
        else
        {
            // Door is lock.
            onOpenDoorEvent?.Invoke();
        }
    }

    async void DisableDoor()
    {
        await this.gameObject.transform.DOScale(Vector3.zero,0.5f).SetEase(Ease.InBounce).AsyncWaitForCompletion();
        this.gameObject.SetActive(false);
    }
}