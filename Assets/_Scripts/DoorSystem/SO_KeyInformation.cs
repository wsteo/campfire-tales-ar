using UnityEngine;

[CreateAssetMenu(fileName = "KeyInformation", menuName = "DoorKeySystem/SO_DoorInformation", order = 0)]
public class SO_KeyInformation : ScriptableObject
{
    [SerializeField]
    private string _name;
    public string Name => _name;

    [SerializeField]
    private string _doorID;
    public string DoorID => _doorID;
}