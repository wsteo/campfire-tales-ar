using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using TMPro;

public class KeyManager : MonoBehaviour
{
    [SerializeField]
    private List<SO_InteractionInfo> _keyInfoList;

    // [SerializeField]
    // private TMP_Text _keyTrackerTextUI;

    // [SerializeField]
    // private IntEventChannelSO _numOfKeyIntEventSO;

    public delegate void UpdateKeyManagerEvent(int numberOfKeys);
    public static event UpdateKeyManagerEvent onUpdateKeyManagerEvent;

    private void OnEnable()
    {
        KeyBehaviour.OnKeyPickUp += AddKeyToInventory;
    }

    private void OnDisable()
    {
        KeyBehaviour.OnKeyPickUp -= AddKeyToInventory;
    }

    public void AddKeyToInventory(SO_InteractionInfo keyInfo)
    {
        _keyInfoList.Add(keyInfo);
        onUpdateKeyManagerEvent?.Invoke(_keyInfoList.Count);
        // DisplayNumberOfKeys(_keyInfoList.Count);
    }

    public void RemoveKeyFromInventory(SO_InteractionInfo keyInfo)
    {
        _keyInfoList.Remove(keyInfo);
        onUpdateKeyManagerEvent?.Invoke(_keyInfoList.Count);
        // DisplayNumberOfKeys(_keyInfoList.Count);
    }

    public bool SearchKeyInInventory(GameObject interactableObject)
    {
        SO_InteractionInfo doorInfo = interactableObject.GetComponent<DoorBehaviour>().DoorInfo;

        foreach (SO_InteractionInfo key in new List<SO_InteractionInfo>(_keyInfoList))
        {
            if (doorInfo.name.Equals(key.name))
            {
                RemoveKeyFromInventory(key);
                Debug.Log("Found key for " + doorInfo.name);
                return true;
            }
        }
        return false;
    }

    // private void DisplayNumberOfKeys(int numberOfKeys)
    // {
    //     _numOfKeyIntEventSO.RaiseEvent(numberOfKeys);
    // }
}