using UnityEngine;

public class KeyBehaviour : MonoBehaviour
{
    public delegate void PickUpKeyEvent(SO_InteractionInfo _interactionInfo);
    public static event PickUpKeyEvent OnKeyPickUp;

    [SerializeField]
    private SO_InteractionInfo _interactionInfo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnKeyPickUp?.Invoke(_interactionInfo);
            Destroy(this.gameObject);
            Debug.Log("Pick up key! " + _interactionInfo.InteractionName);
        }
    }
}