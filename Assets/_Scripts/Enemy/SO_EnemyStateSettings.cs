using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_EnemyStateSettings", menuName = "Enemy/SO_EnemyStateSettings", order = 0)]
public class SO_EnemyStateSettings : ScriptableObject
{
    private float _stateNameSetting;
    
    [SerializeField]
    private float _moveSpeed;
    public float MoveSpeed => _moveSpeed;

    [SerializeField]
    private float _turnSpeed;
    public float TurnSpeed => _turnSpeed;
}