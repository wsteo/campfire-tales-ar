using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class EnemyCollision : MonoBehaviour
{
    // [SerializeField]
    // private GameObject loseUnity;

    // [SerializeField]
    // private VoidEventChannelSO _onLoseEventChannel;

    public delegate void CaughtPlayerEvent();

    public static event CaughtPlayerEvent onCaughtPlayerEvent;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            // other.gameObject.SetActive(false);
            // loseUnity.SetActive(true);
            // _onLoseEventChannel.RaiseEvent();
            onCaughtPlayerEvent?.Invoke();
        }

        // if (other.collider.CompareTag("Box"))
        // {
        //     Destroy(this.gameObject);
        // }

        // if (other.collider.CompareTag("Coin"))
        // {
        //     Destroy(other.gameObject);

        //     Debug.Log("Enemy discovered coin");
        // }
    }
}
