using UnityEngine;
using FSM;
using System.Collections;

namespace EnemyAISystem
{
    public class EnemyChaseState : EnemyBaseState
    {
        public EnemyChaseState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings)
        {
        }

        public override void Enter()
        {
            base.Enter();
            _enemy.Animator.SetBool("isRunning", true);
        }

        public override EnemyStates StateType => EnemyStates.Chase;

        public override void LogicUpdate()
        {
            _enemy.NavAgent.SetDestination(_enemy.Player.position);

            if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.MEDIUM)
            {
                _enemyStateMachine.ChangeState(_enemy.investigate);
            }
        }

        public override void PhysicsUpdate() { }
        // public void EnterState(EnemyStateMachine enemy)
        // {
        //     enemy.NavAgent.speed = 6f;
        //     enemy.NavAgent.angularSpeed = 300f;
        //     enemy.FOV.ViewAngle = 120f;
        // }

        // public void UpdateState(EnemyStateMachine enemy)
        // {
        //     enemy.NavAgent.SetDestination(enemy.Player.position);

        //     if (!enemy.isTargetVisible())
        //     {
        //         enemy.SwitchState(enemy.patrol);
        //     }
        // }

        // public void OnCollisionState(EnemyStateMachine enemy)
        // {
        //     throw new System.NotImplementedException();
        // }
    }
}