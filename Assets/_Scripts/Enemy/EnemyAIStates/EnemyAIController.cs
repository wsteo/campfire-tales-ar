using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
using Gameplay.FOV;
using FSM;
using System;

namespace EnemyAISystem
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(FieldOfView))]
    public class EnemyAIController : MonoBehaviour
    {
        [Header("General")]
        [SerializeField] private FieldOfView _fov;
        public FieldOfView FOV { get => _fov; }

        [SerializeField] private NavMeshAgent _navAgent;
        public NavMeshAgent NavAgent { get => _navAgent; set => _navAgent = value; }

        [SerializeField] private Animator _animator;
        public Animator Animator { get => _animator; }

        [SerializeField] private Transform _player;
        public Transform Player { get => _player; }

        [Range(0, 100)] public float suspiciousLevel;


        [Header("Patrol State")]
        [SerializeField] private Transform[] _points;
        public Transform[] Points { get => _points; }


        [Header("Alert State")]
        [SerializeField] private Transform _alertNotification;
        public Transform AlertNotification { get => _alertNotification; }


        [Header("Distraction System")]
        [SerializeField] private bool _isDistracted;
        public bool IsDistracted { get => _isDistracted; set => _isDistracted = value; }
        public Transform distractionPostPoint { get; set; }


        [Header("Player Hiding System")]
        [SerializeField] private bool _isPlayerHidden;
        public bool IsPlayerHidden { get => _isPlayerHidden; set => _isPlayerHidden = value; }

        public EnemyStateMachine enemyAIStateMachine { get; private set; }
        public EnemyPatrolState patrol { get; private set; }
        public EnemyIdleState idle { get; private set; }
        public EnemyAlertState alert { get; private set; }
        public EnemyInvestigateState investigate { get; private set; }
        public EnemyChaseState chase { get; private set; }
        public EnemyCombatState combat { get; private set; }


        [SerializeField]
        private EnemyBaseState.EnemyStates _initialState;

        public EnemyBaseState.EnemyStates InitialState => _initialState;

        [Header("State Settings")]
        [SerializeField] private SO_EnemyStateSettings _patrolStateSettings;
        [SerializeField] private SO_EnemyStateSettings _idleStateSettings;
        [SerializeField] private SO_EnemyStateSettings _alertStateSettings;
        [SerializeField]private SO_EnemyStateSettings _investigateStateSettings;
        [SerializeField] private SO_EnemyStateSettings _chaseStateSettings;
        [SerializeField] private SO_EnemyStateSettings _combatStateSettings;

        [Header("Interation")]
        [SerializeField]
        private InteractionRangeChecker _interactionRangeChecker;
        public InteractionRangeChecker InteractionRangeChecker => _interactionRangeChecker;

        // [SerializeField]
        // private VoidEventChannelSO _onPlayerIsVisibleEvent;
        // public VoidEventChannelSO OnPlayerIsVisibleEvent => _onPlayerIsVisibleEvent;

        private void OnEnable()
        {
            DistractionBehaviour.SendSignalEvent += DistractedAction;
        }

        private void OnDisable()
        {
            DistractionBehaviour.SendSignalEvent -= DistractedAction;
        }

        private void DistractedAction(GameObject distractionPost)
        {
            _isDistracted = true;
            distractionPostPoint = distractionPost.transform;
            enemyAIStateMachine.ChangeState(investigate);
        }

        private void Awake()
        {
            _interactionRangeChecker = GetComponent<InteractionRangeChecker>();
        }

        private void Start()
        {
            if (_player == null)
                _player = GameObject.FindGameObjectWithTag("Player").transform;

            if (_navAgent == null)
                _navAgent = GetComponent<NavMeshAgent>();

            if (_fov == null)
                _fov = GetComponent<FieldOfView>();

            
            AlertNotification.localScale = new Vector3(0f,0f,0f);
            
            enemyAIStateMachine = new EnemyStateMachine();
            idle = new EnemyIdleState(this, enemyAIStateMachine, _idleStateSettings);
            patrol = new EnemyPatrolState(this, enemyAIStateMachine, _patrolStateSettings);
            alert = new EnemyAlertState(this, enemyAIStateMachine, _alertStateSettings);
            investigate = new EnemyInvestigateState(this, enemyAIStateMachine, _investigateStateSettings);
            chase = new EnemyChaseState(this, enemyAIStateMachine, _chaseStateSettings);
            // combat = new EnemyCombatState(this, enemyAIStateMachine, _combatStateSettings);

            if (_initialState == EnemyBaseState.EnemyStates.Idle)
            {
                enemyAIStateMachine.Initialize(idle);
            }
            else if (_initialState == EnemyBaseState.EnemyStates.Patrol)
            {
                enemyAIStateMachine.Initialize(patrol);
            }

            StartCoroutine(CoroutineUpdate());
        }

        private void Update()
        {
            enemyAIStateMachine.CurrentState.LogicUpdate();

        }

        private void FixedUpdate()
        {
            enemyAIStateMachine.CurrentState.PhysicsUpdate();
        }

        #region SuspicionSystem

        public delegate void PlayerDetectedEvent();
        public static event PlayerDetectedEvent onPlayerDetectedEvent;

        public enum SuspicionLevel
        {
            LOW,
            MEDIUM,
            HIGH,
            EXPOSED
        }

        public EnemyAIController.SuspicionLevel CurrentSuspicionLevel { get; private set; }

        public void IncreaseSuspicionLevel(float suspicionPoint)
        {
            suspiciousLevel = Mathf.Min(100f, suspiciousLevel + suspicionPoint);
        }

        public void DecreaseSuspicionLevel(float suspicionPoint)
        {
            suspiciousLevel = Mathf.Max(0f, suspiciousLevel - suspicionPoint);
        }

        public IEnumerator CoroutineUpdate()
        {
            while (enemyAIStateMachine.IsIdle || enemyAIStateMachine.IsPatrolling || enemyAIStateMachine.IsAlert || enemyAIStateMachine.IsInvestigate || enemyAIStateMachine.IsChase)
            {
                float frequency = 0.1f;
                yield return new WaitForSeconds(frequency);
                float distanceToTarget = FOV.GetTargetDistance(out Transform target);
                if (distanceToTarget > 0f && !IsPlayerHidden)
                {
                    IncreaseSuspicionLevel((30f / distanceToTarget) * frequency);
                }
                else
                {
                    DecreaseSuspicionLevel(10f * frequency);
                }

                CurrentSuspicionLevel = CheckSuspicionLevel();
                Debug.Log("Current Suspicion Level: " + CurrentSuspicionLevel.ToString());
            }
        }

        public SuspicionLevel CheckSuspicionLevel()
        {
            if (suspiciousLevel > 90)
                return SuspicionLevel.EXPOSED;
            else if (suspiciousLevel > 50)
                return SuspicionLevel.HIGH;
            else if (suspiciousLevel > 5)
                return SuspicionLevel.MEDIUM;
            else
                return SuspicionLevel.LOW;
        }

        public void PingSuspicionLevel()
        {
            onPlayerDetectedEvent?.Invoke();
        }
        #endregion

        #region HelperMethod
        public void SwitchToInitialState()
        {
            AlertNotification.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBounce);
            if (InitialState == EnemyBaseState.EnemyStates.Idle)
            {
                enemyAIStateMachine.ChangeState(idle);
            }
            else if (InitialState == EnemyBaseState.EnemyStates.Patrol)
            {
                enemyAIStateMachine.ChangeState(patrol);
            }
        }
        #endregion
    }
}
