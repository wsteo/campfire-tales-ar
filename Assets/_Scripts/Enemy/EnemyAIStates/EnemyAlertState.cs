using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using FSM;

namespace EnemyAISystem
{
    public class EnemyAlertState : EnemyBaseState
    {
        public override EnemyStates StateType => EnemyStates.Alert;

        public EnemyAlertState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings)
        {
        }

        public override void Enter()
        {
            base.Enter();
            StartAlertNotification();
            _enemy.PingSuspicionLevel();
            // _enemy.transform.DOLocalMoveY(0.5f, 1f).SetEase(Ease.InOutBounce);
            _enemy.Animator.Play("Jump");
            _enemy.Animator.SetBool("isAlert", true);
            _enemy.Animator.SetBool("isWalking", true);
        }

        public override void LogicUpdate()
        {
            // _enemy.PlayerVisibilityStatusUI.UpdateVisibilityUIToAlert();
            Debug.Log("Alert State");

            if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.EXPOSED)
            {
                _enemyStateMachine.ChangeState(_enemy.chase);
            }
            else if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.MEDIUM)
            {
                _enemyStateMachine.ChangeState(_enemy.investigate);
            }
            else if (_enemy.IsDistracted && (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.MEDIUM))
            {
                _enemyStateMachine.ChangeState(_enemy.investigate);
            }
            else if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.LOW)
            {
                _enemy.SwitchToInitialState();
            }
        }

        public override void PhysicsUpdate() { }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override void Exit()
        {
            base.Exit();
        }

        private async void StartAlertNotification()
        {

            await _enemy.AlertNotification.DOScale(new Vector3(2.5f, 2.5f, 2.5f), 0.5f).SetEase(Ease.InElastic).AsyncWaitForCompletion();

            _enemy.AlertNotification.DOMoveY(_enemy.transform.position.y + 0.05f, 0.3f).SetEase(Ease.InBounce).SetLoops(-1, LoopType.Yoyo);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
