using UnityEngine;
using EnemyAISystem;

namespace FSM
{
    public class EnemyStateMachine : StateMachine<EnemyBaseState>
    {   
        public bool IsPatrolling => CurrentState.StateType == EnemyBaseState.EnemyStates.Patrol;
        public bool IsAlert => CurrentState.StateType == EnemyBaseState.EnemyStates.Alert;
        public bool IsIdle => CurrentState.StateType == EnemyBaseState.EnemyStates.Idle;
        public bool IsInvestigate => CurrentState.StateType == EnemyBaseState.EnemyStates.Investigate;
        public bool IsChase => CurrentState.StateType == EnemyBaseState.EnemyStates.Chase;
        public bool IsCombat => CurrentState.StateType == EnemyBaseState.EnemyStates.Combat;
    }
}