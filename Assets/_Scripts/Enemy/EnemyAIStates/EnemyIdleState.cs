using System;
using UnityEngine;
using FSM;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EnemyAISystem
{
    public class EnemyIdleState : EnemyBaseState
    {
        private Transform currentEnemyTransform;

        private bool isArrivedAtStartingPoint;

        public EnemyIdleState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings) { }

        public override EnemyStates StateType => EnemyStates.Idle;

        public override void Enter()
        {
            base.Enter();


            Vector3 startingPoint = _enemy.Points[0].position;
            Vector3 distBetweenEnemyAndTarget = _enemy.transform.position - startingPoint;
            if (distBetweenEnemyAndTarget.magnitude > 0.1f)
            {
                MoveToStartingPoint();
            }
        }
        public override void Exit()
        {
            base.Exit();
        }

        public override void LogicUpdate()
        {
            Debug.Log("Idle State");

            // MoveToStartingPoint();
            if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.MEDIUM)
                _enemyStateMachine.ChangeState(_enemy.alert);
            else if (_enemy.IsDistracted && (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.LOW))
                _enemyStateMachine.ChangeState(_enemy.investigate);
        }

        public override void PhysicsUpdate() { }

        private void MoveToStartingPoint()
        {
            if (_enemy.Points.Length == 0)
                return;

            Vector3 startingPoint = _enemy.Points[0].position;
            _enemy.NavAgent.destination = startingPoint;

            Vector3 distBetweenEnemyAndTarget = _enemy.transform.position - startingPoint;

            if (distBetweenEnemyAndTarget.magnitude < 0.1f)
            {
                isArrivedAtStartingPoint = true;
                _enemy.Animator.SetBool("isWalking", false);
            }
        }
    }
}