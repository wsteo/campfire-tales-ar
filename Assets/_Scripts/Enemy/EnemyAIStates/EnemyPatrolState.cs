using UnityEngine;
using UnityEngine.AI;
using System;
using System.Collections;
using System.Collections.Generic;
using FSM;

namespace EnemyAISystem
{
    public class EnemyPatrolState : EnemyBaseState
    {
        private int destPoint;

        public EnemyPatrolState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings) { }

        public override EnemyStates StateType => EnemyStates.Patrol;

        public override void Enter()
        {
            base.Enter();

            destPoint = GetNearestPoint();
        }

        public override void LogicUpdate()
        {
            Debug.Log("Patrolling!");

            if (!_enemy.NavAgent.pathPending && _enemy.NavAgent.remainingDistance < 0.5f)
                GoToNextPoint();

            if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.MEDIUM)
            {
                _enemyStateMachine.ChangeState(_enemy.alert);
            }
            else if (_enemy.IsDistracted && (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.LOW))
                _enemyStateMachine.ChangeState(_enemy.investigate);
        }

        public override void PhysicsUpdate() { }

        private void GoToNextPoint()
        {
            if (_enemy.Points.Length == 0)
                return;

            _enemy.NavAgent.destination = _enemy.Points[destPoint].position;

            destPoint = (destPoint + 1) % _enemy.Points.Length;

            // Animation
            _enemy.Animator.SetBool("isWalking", true);
        }

        private int GetNearestPoint()
        {
            if (_enemy.Points.Length == 0)
                return -1;

            float shortestDst = Mathf.Infinity;
            int shortestPathIndex = -1;

            for (int i = 0; i < _enemy.Points.Length; i++)
            {
                float dstBetweenEnemyAndPoint = Vector3.Distance(_enemy.Points[i].position, _enemy.transform.position);

                if (dstBetweenEnemyAndPoint <= shortestDst)
                {
                    shortestPathIndex = i;
                    shortestDst = dstBetweenEnemyAndPoint;
                }
            }

            return shortestPathIndex;
        }
    }
}
