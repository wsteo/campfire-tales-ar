using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using FSM;

namespace EnemyAISystem
{
    public class EnemyInvestigateState : EnemyBaseState
    {
        private Transform _targetLastSeenLocation;

        private bool _fixDistraction;

        public EnemyInvestigateState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings)
        {
        }

        public override EnemyStates StateType => EnemyStates.Investigate;

        public override void Enter()
        {
            base.Enter();
            
            _enemy.Animator.SetBool("isWalking", true);
            _fixDistraction = false;

            if (_enemy.IsDistracted)
            {
                _targetLastSeenLocation = _enemy.distractionPostPoint;
            }
            else
            {
                _targetLastSeenLocation = _enemy.Player.transform;
            }
            
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void LogicUpdate()
        {
            Debug.Log("Investigate State");
            _enemy.NavAgent.SetDestination(_targetLastSeenLocation.position);

            Vector3 distanceToTarget = _enemy.transform.position - _targetLastSeenLocation.position;

            if (distanceToTarget.magnitude < 1.5f && _enemy.IsDistracted)
            {
                FixDistraction(3f);
            }

            if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.HIGH)
            {
                _enemyStateMachine.ChangeState(_enemy.chase);
            }
            else if (_enemy.CurrentSuspicionLevel == EnemyAIController.SuspicionLevel.LOW)
            {
                _enemy.SwitchToInitialState();
            }    
        }

        public override void PhysicsUpdate()
        {
        }

        // public delegate void DisableDistractionEvent();
        // public static event DisableDistractionEvent OnDisableDistractionEvent;

        // public void DisableDistraction()
        // {
        //     _fixDistraction = true;
        //     OnDisableDistractionEvent?.Invoke();
        // }

        public async void FixDistraction(float time)
        {
            Debug.Log($"Wait for {time} second...");

            _enemy.NavAgent.speed = 0f;
            _enemy.Animator.SetBool("isWalking", false);
            GameObject interactable = _enemy.InteractionRangeChecker.InteractableObject;

            await Task.Delay(TimeSpan.FromSeconds(time));
            if (!_fixDistraction)
            {
                interactable.GetComponent<I_Interactable>().Interact();
                _enemy.IsDistracted = false;
                _fixDistraction = true;
                Debug.Log("Distraction Fixed!");

                _enemy.SwitchToInitialState();
            }
        }


    }

}