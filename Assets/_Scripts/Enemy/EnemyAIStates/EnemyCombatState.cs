using System;
using UnityEngine;
using FSM;
using System.Collections;

namespace EnemyAISystem
{
    public class EnemyCombatState: EnemyBaseState
    {
        public EnemyCombatState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings) : base(enemy, enemyStateMachine, enemyStateSettings)
        {
        }

        public override EnemyStates StateType => EnemyStates.Combat;

        public override void LogicUpdate()
        {
        }

        public override void PhysicsUpdate()
        {
        }
    }

}