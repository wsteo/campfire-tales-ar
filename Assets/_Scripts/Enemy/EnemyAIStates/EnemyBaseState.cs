using System;
using UnityEngine;
using System.Collections;
using EnemyAISystem;

namespace FSM
{
    public abstract class EnemyBaseState : BaseState
    {
        protected EnemyStateMachine _enemyStateMachine;
        protected EnemyAIController _enemy;
        protected SO_EnemyStateSettings _enemyStateSettings;
        public EnemyBaseState(EnemyAIController enemy, EnemyStateMachine enemyStateMachine, SO_EnemyStateSettings enemyStateSettings)
        {
            _enemy = enemy;
            _enemyStateMachine = enemyStateMachine;
            _enemyStateSettings = enemyStateSettings;
        }

        public enum EnemyStates
        {
            Null,
            Patrol,
            Idle,
            Alert,
            Investigate,
            Chase,
            Combat
        }

        public abstract EnemyStates StateType { get; }

        public override void Enter()
        {
            base.Enter();
            _enemy.NavAgent.speed = _enemyStateSettings.MoveSpeed;
            _enemy.NavAgent.angularSpeed = _enemyStateSettings.TurnSpeed;
        }

        public override void LogicUpdate()
        {

        }
        public override void PhysicsUpdate()
        {

        }
    }
}